from django.db import models

class Categorie(models.Model):
    type = models.CharField(max_length=100, null=True, blank=False)
    date = models.DateField(null=True, blank=False)
    description = models.TextField(default="")
    number = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.type